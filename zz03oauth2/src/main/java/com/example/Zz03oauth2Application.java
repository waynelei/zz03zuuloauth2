package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.ManagementServerProperties.SessionCreationPolicy;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;

@SpringBootApplication
@EnableDiscoveryClient

//@EnableZuulProxy
@EnableResourceServer
@EnableAuthorizationServer
public class Zz03oauth2Application {

    public static void main(String[] args) {
        SpringApplication.run(Zz03oauth2Application.class, args);
    }

    @Configuration
    protected static class OAuthSecurityConfig extends AuthorizationServerConfigurerAdapter {

        @Autowired
        private AuthenticationManager authenticationManager;

        @Override
        public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
            endpoints.authenticationManager(authenticationManager);
        }

        @Override
        public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
            oauthServer.checkTokenAccess("isAuthenticated()");
        }

        @Override
        public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
            clients.inMemory()
                    .withClient("clientId")
                    .secret("secretId")
                    .authorizedGrantTypes("authorization_code", "client_credentials")
                    .scopes("app");
        }
    }

    @Configuration
    protected static class RestSecurityConfig extends WebSecurityConfigurerAdapter {

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.anonymous().disable()
//                    .sessionManagement()
//                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
//                    .and()
                    .exceptionHandling()
                    //                    .accessDeniedHandler(accessDeniedHandler()) // handle access denied in general (for example comming from @PreAuthorization
                    //                    .authenticationEntryPoint(entryPointBean()) // handle authentication exceptions for unauthorized calls.
                    .and()
                    .authorizeRequests()
                    //                    .antMatchers("/hystrix.stream/**", "/info", "/error").permitAll()
                    .anyRequest().authenticated().and().csrf().disable();
        }

//        @Bean
//        @Autowired
//        AccessDeniedHandler accessDeniedHandler() {
//            return new AccessDeniedExceptionHandler();
//        }
//
//        @Bean
//        @Autowired
//        AuthenticationEntryPoint entryPointBean() {
//            return new UnauthorizedEntryPoint();
//        }
        // 不需要权限控制的路径
        @Override
        public void configure(WebSecurity web) throws Exception {
            web.ignoring().antMatchers("/hystrix.stream/**", "/info", "/error");
        }
    }
}
